const path = require('path');

module.exports = {
  siteMetadata: {
    title: 'Kendall Roth - Portfolio',
  },
  plugins: [
    {
      resolve: 'gatsby-plugin-nprogress',
      options: {
        color: 'tomato',
        showSpinner: false,
      },
    },
    {
      resolve: 'gatsby-plugin-typography',
      options: {
        pathToConfigModule: 'src/utils/typography.js',
      },
    },
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-remove-trailing-slashes',
    'gatsby-plugin-sharp',
    'gatsby-plugin-styled-components',
    // Source plugins
    //   NOTE: Must have most-specific paths above less-specific paths
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        // Data -> Images
        name: 'images',
        path: path.join(__dirname, '/src/data/images'),
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        // Data -> Types (mimics CMS)
        name: 'types',
        path: path.join(__dirname, '/src/data/types'),
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        // Data -> General
        name: 'data',
        path: path.join(__dirname, '/src/data'),
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'pages',
        path: path.join(__dirname, '/src/pages/'),
      },
    },
    // Transformer plugins
    'gatsby-transformer-json',
    'gatsby-transformer-remark',
    'gatsby-transformer-sharp',
  ],
};
