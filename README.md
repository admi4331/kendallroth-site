# KendallRoth Portfolio Site

Portfolio site for Kendall Roth built with Gatsby static site generator, replacing my old site (PHP) from my initial days as a developer.

[![Netlify Status](https://api.netlify.com/api/v1/badges/ca7ee544-fdcf-4e8f-8b02-afeb2fe51fc8/deploy-status)](https://app.netlify.com/sites/kendallroth/deploys)

## Development

```bash
# Start project locally
yarn app:develop

# Formatting and style checks
yarn app:eslint
yarn app:prettier
```

### Notes

- Technology card images should be converted to PNG format, resized to 100px x 100px, and down-sampled before being added

## Production

```bash
# Build project assets
yarn app:build

# Publish project with surge
yarn app:deploy
```

## Credits

- Taken from [Gatsby Default Starter](https://github.com/gatsbyjs/gatsby-starter-default)
