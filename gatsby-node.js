const path = require('path');

const Paths = {
  src: path.join(__dirname, 'src'),
};

/**
 * Modify the Gatsby webpack config
 * @param {props} props.actions - Webpack Redux actions
 * @param {props} props.stage   - Gatsby build stage
 */
exports.onCreateWebpackConfig = ({ actions }) => {
  // Add webpack support for aliases
  actions.setWebpackConfig({
    resolve: {
      alias: {
        '@assets': path.join(Paths.src, 'assets'),
        '@components': path.join(Paths.src, 'components'),
        '@data': path.join(Paths.src, 'data'),
        '@utils': path.join(Paths.src, 'utils'),
        '@layouts': path.join(Paths.src, 'layouts'),
        '@pages': path.join(Paths.src, 'pageComponents'),
        '@src': path.join(Paths.src),
      },
    },
  });
};
