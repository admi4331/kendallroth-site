import React from 'react';
import { graphql } from 'gatsby';

// Components
import IndexPage from '@pages/index';

const IndexQuery = ({ data }) => <IndexPage data={data} />;

export const query = graphql`
  query IndexQuery {
    # Employment files/images
    allEmployerFiles: allFile(
      filter: {
        sourceInstanceName: { eq: "types" }
        relativeDirectory: { eq: "employers" }
      }
    ) {
      ...EmployerSummaryFileFragment
    }
    allEmployerImages: allFile(
      filter: {
        sourceInstanceName: { eq: "images" }
        relativeDirectory: { eq: "employers" }
      }
    ) {
      ...EmployerSummaryImageFragment
    }

    # Project files/images
    allProjectFiles: allFile(
      filter: {
        sourceInstanceName: { eq: "types" }
        relativeDirectory: { eq: "projects" }
      }
    ) {
      ...ProjectSummaryFileFragment
    }
    allProjectImages: allFile(
      filter: {
        sourceInstanceName: { eq: "images" }
        relativeDirectory: { eq: "projects" }
      }
    ) {
      ...ProjectSummaryImageFragment
    }
  }
`;

export default IndexQuery;

/* eslint react/prop-types: off */
