---
collaborators:
  - doug-epp
  - tristan-freitas
image: capstone.png
published: true
slug: capstone
status: Completed
title: Capstone Project
year: 2018
---

OurGroup was designed and built by several Conestoga College students as a Capstone project in the final year of the Computer Programmer/Analyst program.

> Keeping a group up-to-date should be easy!

## Collaborators

- [Doug Epp](https://github.com/DougEpp)
- [Kendall Roth](https://github.com/kendallroth)
- [Tristan Freitas](https://github.com/freitastristan)
