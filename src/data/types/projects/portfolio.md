---
collaborators: []
image: portfolio_site.png
published: true
slug: portfolio
status: Active
title: Portfolio
year: 2018
---

My portfolio site has been work in progress for as long as I have been a developer. Each progressive iteration has explored a new technology (at the time) as I was learning, much to my current-self's chagrin. However, it has proven a valuable learning resource each time, allowing me to grow more comfortable with my skills and preferences.

## Stages

1. HTML and CSS
   - The initial site was a (_non-responsive_) attempt at demonstrating at least a basic web prowess with HTML and CSS..._enough said_.
2. PHP
   - The change to PHP brought an awareness of designing a responsive website, done utilizing the Foundation grid framework. Additionally, I experimented with fragments and eventually reworked the entire site to use a custom-built MVC approach (primarily for pretty URLs).
3. React
   - As I entered the "real-world" through my co-op experiences I realized that SPAs were the up-and-coming star of the web world. Determined to jump on board, I focused on React for several months, learning to love its slight idiosyncrasies and creating my own structure and practices. I've been working with React for over a year now, both at work and in play, and decided to experiment with Gatsby for hosting my static portfolio on Netlify.
