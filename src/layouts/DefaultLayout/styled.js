import styled from 'styled-components';

/**
 * Main content container
 */
const GatsbyBodySC = styled.section`
  min-height: 100vh;
  display: flex;
  flex-direction: column;
`;

export { GatsbyBodySC };

/* eslint import/prefer-default-export: off */
