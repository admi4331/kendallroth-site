import styled from 'styled-components';

/**
 * Icon image
 * @param {string}  color - Icon background color
 * @param {boolean} round - Whether icon is round
 */
const IconImageSC = styled.img`
  width: ${({ size }) => size}px;
  height: ${({ size }) => size}px;
  margin: 0;
`;

/**
 * Icon link
 */
const IconLinkSC = styled.a`
  display: block;
  padding: ${({ theme }) => theme.spacing * 1.5}px;
  text-decoration: none;
  background-color: ${({ color }) => color};
  border-radius: ${({ round }) => (round ? 'border-radius: 100%' : '2px')};

  transition: background-color 0.1s;

  &:hover {
    background-color: ${({ theme }) => theme.palette.primary.main};
  }
`;

/**
 * Icon bar wrapper
 * NOTE: Out of order for dependencies
 */
const IconBarSC = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin-bottom: 1.25em;

  ${IconLinkSC}:not(:first-of-type) {
    margin-left: ${({ theme }) => theme.spacing * 4}px;
  }
`;

export { IconBarSC, IconImageSC, IconLinkSC };
