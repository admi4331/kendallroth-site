import styled, { css } from 'styled-components';
import Img from 'gatsby-image';

// Utilities
import { MediaQuery } from '@utils/mediaQuery';

/**
 * Panel header left content (company and position)
 */
const HeaderContentLeftSC = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;

  ${MediaQuery.phoneOnly``};
`;

/**
 * Panel header right content (dates)
 */
const HeaderContentRightSC = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-left: auto;
  color: #616161;
  font-size: 0.9em;

  ${MediaQuery.phoneOnly`
    margin-left: 0;
    font-style: italic;
  `};
`;

/**
 * Employment panel content
 */
const PanelContentSC = styled.div`
  width: calc(100% - 16px);
  padding: 1em;
  background-color: rgba(255, 255, 255, 0.9);
  border-radius: 0 0 4px 4px;
  box-shadow: ${({ theme }) => theme.shadows[1]};
`;

/**
 * Employment panel header
 */
const PanelHeaderSC = styled.header`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 100%;
  padding: 0.75em 1em;
  background-color: white;
  border-radius: 4px;
  box-shadow: ${({ theme }) => theme.shadows[2]};
  transition: box-shadow 0.15s ease-in-out;
  z-index: 2;

  &:hover {
    box-shadow: ${({ theme }) => theme.shadows[4]};
  }
`;

/**
 * Employment panel header content
 */
const PanelHeaderContentSC = styled.div`
  display: flex;
  flex-direction: row;
  flex-grow: 1;

  ${MediaQuery.phoneOnly`
    flex-direction: column;
  `};
`;

/**
 * Employment panel header logo
 */
const PanelLogoSC = styled(Img)`
  width: 100%;
  max-width: 50px;
  max-height: 50px;
  margin: 0;
  margin-right: 1em;
  border-radius: 4px;
`;

/**
 * Employment panel logo placeholder (with no image)
 */
const PanelLogoPlaceholderSC = styled.div`
  width: 50px;
  height: 50px;
  margin-right: 1em;
  border: 2px dashed lightgrey;
  background-color: rgba(0, 0, 0, 0.05);
  border-radius: 4px;
`;

/**
 * Employment panel header subtitle
 */
const PanelSubtitleSC = styled.span`
  color: #616161;
  font-size: 0.9em;
`;

/**
 * Employment panel header title
 */
const PanelTitleSC = styled.div`
  margin-right: 0.5em;
  font-size: 1.25em;
  font-weight: bold;
`;

/**
 * Employment panel toggle icon
 */
const PanelToggleIconSC = styled.span`
  display: flex;
  align-items: center;
  align-content: center;
  width: 30px;
  height: 30px;
  margin-left: 1em;
  cursor: pointer;
`;

/**
 * Employment panel and description
 *   NOTE: Must be underneath "class" dependencies
 * @param {boolean} disabled - Whether panel is disabled
 */
const EmploymentPanelSC = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  margin-bottom: 1.5em;

  ${({ disabled }) =>
    disabled &&
    css`
      cursor: not-allowed;

      ${PanelHeaderSC} {
        color: white;
        background-color: rgba(200, 200, 200, 0.5);
      }
      ${PanelLogoPlaceholderSC} {
        border-color: white;
      }
      ${PanelSubtitleSC} {
        color: white;
      }
      ${HeaderContentRightSC} {
        color: white;
      }
    `};
`;

export {
  EmploymentPanelSC,
  HeaderContentLeftSC,
  HeaderContentRightSC,
  PanelContentSC,
  PanelHeaderSC,
  PanelHeaderContentSC,
  PanelLogoSC,
  PanelLogoPlaceholderSC,
  PanelSubtitleSC,
  PanelTitleSC,
  PanelToggleIconSC,
};
