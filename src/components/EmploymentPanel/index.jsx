import React from 'react';
import PropTypes from 'prop-types';

// Styles
import {
  EmploymentPanelSC,
  HeaderContentLeftSC,
  HeaderContentRightSC,
  PanelContentSC,
  PanelHeaderSC,
  PanelHeaderContentSC,
  PanelLogoSC,
  PanelLogoPlaceholderSC,
  PanelSubtitleSC,
  PanelTitleSC,
  PanelToggleIconSC,
} from './styled';

/**
 * Employment description and tasks card
 * @param {string}  className      - Component class
 * @param {string}  company        - Employer company name
 * @param {object}  dates          - Position start/end dates
 * @param {boolean} hirePrompt     - Whether the card is a hire prompt
 * @param {Object}  imageSize      - Company logo image (gatsby-image)
 * @param {string}  position       - Position/role within company
 */
const EmploymentPanel = (props) => {
  const { className, company, dates, hirePrompt, imageSize, position } = props;

  const hasImage = Boolean(imageSize) && Boolean(imageSize);

  return (
    <EmploymentPanelSC className={className} disabled={hirePrompt}>
      <PanelHeaderSC>
        {hasImage ? (
          <PanelLogoSC sizes={imageSize} width="50" />
        ) : (
          <PanelLogoPlaceholderSC />
        )}
        <PanelHeaderContentSC>
          <HeaderContentLeftSC>
            <PanelTitleSC>{company}</PanelTitleSC>
            <PanelSubtitleSC>{position}</PanelSubtitleSC>
          </HeaderContentLeftSC>
          <HeaderContentRightSC>
            {dates.start}&thinsp;&ndash;&thinsp;{dates.end || 'Present'}
          </HeaderContentRightSC>
        </PanelHeaderContentSC>
        {/* <PanelToggleIconSC>
          {!isContactPlaceholder ? (
            <i className="material-icons">question</i>
          ) : (
            <i className="material-icons">add</i>
          )}
        </PanelToggleIconSC> */}
      </PanelHeaderSC>
    </EmploymentPanelSC>
  );
};

EmploymentPanel.propTypes = {
  className: PropTypes.string,
  company: PropTypes.string.isRequired,
  dates: PropTypes.shape({
    end: PropTypes.string,
    start: PropTypes.string.isRequired,
  }).isRequired,
  hirePrompt: PropTypes.bool,
  imageSize: PropTypes.shape({
    originalName: PropTypes.string.isRequired,
  }),
  position: PropTypes.string.isRequired,
};

EmploymentPanel.defaultProps = {
  className: '',
  hirePrompt: false,
  imageSize: null,
};

export default EmploymentPanel;
