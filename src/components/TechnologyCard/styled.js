import styled, { css } from 'styled-components';
import { lighten } from 'polished';

// Utils
import { MediaQuery } from '@utils/mediaQuery';

/**
 * Progress bar indicator
 */
const ProgressBarSC = styled.div`
  height: 100%;
  background-color: ${({ theme }) => theme.palette.secondary.light};
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
`;

/**
 * Progress bar row
 */
const ProgressRowSC = styled.span`
  position: absolute;
  bottom: 0;
  right: 0;
  left: 0;
  height: 4px;
  background-color: #fce4ec;
  background-color: ${({ theme }) =>
    lighten(0.35, theme.palette.secondary.light)};
`;

/**
 * Technology (skills) card wrapper
 */
const TechnologyCardSC = styled.div`
  position: relative;
  overflow: hidden;
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 0.5em;
  padding-bottom: calc(0.5em + 4px);
  background-color: rgba(255, 255, 255, 0.95);
  border-radius: 4px;
  box-shadow: ${({ theme }) => theme.shadows[2]};
  transition: box-shadow 0.15s ease-in-out;

  ${({ selected }) =>
    selected &&
    css`
      box-shadow: 0px 0px 4px 4px ${({ theme }) => theme.palette.primary.main};
    `};

  &:hover {
    box-shadow: ${({ theme }) => theme.shadows[6]};
  }

  ${MediaQuery.desktopUp`
    padding: 0.75em;
  `};
`;

/**
 * Card content (title, description, etc)
 */
const TechnologyCardContentSC = styled.div`
  display: flex;
  flex-direction: column;
`;

/**
 * Card media image
 */
const TechnologyCardImageSC = styled.img`
  max-height: 48px;
  max-width: 48px;
  margin: 0;
  margin-right: 0.75em;

  /* Fix "broken" or missing images with a placeholder image */
  &[data-broken='true'] {
    padding: 12px;
    background-color: #c5cae9;
    border-radius: 100%;
  }
`;

/**
 * Card description tag
 */
const TechnologyCardTagSC = styled.div`
  padding: 0 0.5em;
  font-size: 12px;
  border-radius: 4px;
  background-color: #e8eaf6;
  border: 1px solid #9fa8da;
`;

/**
 * Card description tags
 */
const TechnologyCardTagsSC = styled.div`
  display: flex;
  flex-direction: row;
  margin-top: 0.25em;

  ${TechnologyCardTagSC}:not(:first-of-type) {
    margin-left: 0.75em;
  }
`;

/**
 * Card content text
 */
const TechnologyCardTextSC = styled.div`
  font-size: 0.9em;
`;

/**
 * Card content title
 */
const TechnologyCardTitleSC = styled.div`
  font-weight: bold;
`;

export {
  ProgressBarSC,
  ProgressRowSC,
  TechnologyCardSC,
  TechnologyCardContentSC,
  TechnologyCardImageSC,
  TechnologyCardTagSC,
  TechnologyCardTagsSC,
  TechnologyCardTextSC,
  TechnologyCardTitleSC,
};
