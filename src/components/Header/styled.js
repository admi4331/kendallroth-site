import styled from 'styled-components';

// Utilities
import theme from '@utils/theme';

// Styles
import { HeaderMenuLinkSC } from './HeaderMenuLink/styled';

const HEADER_HEIGHT_RATIO = 3 / 3;
const imageSize = theme.header.height
  ? theme.header.height * HEADER_HEIGHT_RATIO
  : 40;

/**
 * Header component wrapper
 */
const HeaderSC = styled.header`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  height: ${theme.header.height}px;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0.5rem;
  font-size: 1rem;
  background-color: #263238;
  border-bottom: 4px solid ${theme.palette.primary.light};
  z-index: 100;
`;

/**
 * Header logo
 */
const HeaderLogoSC = styled.img`
  position: absolute;
  bottom: -${imageSize / 2}px;
  left: calc(50% - ${imageSize / 2}px);
  width: ${imageSize}px;
  height: ${imageSize}px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0;
  padding: 4px;
  color: white;
  background-color: ${theme.palette.primary.light};
  border-radius: 100%;
  cursor: default;
`;

/**
 * Header menu left section (of logo)
 */
const HeaderMenuLeftSC = styled.div`
  position: absolute;
  transform: translateX(
    calc(-50% - ${imageSize / 2}px - ${theme.spacing * 4}px)
  );

  ${HeaderMenuLinkSC}:not(:first-of-type) {
    margin-left: 2em;
  }
`;

/**
 * Header menu right section (of logo)
 */
const HeaderMenuRightSC = styled.div`
  position: absolute;
  transform: translateX(
    calc(50% + ${imageSize / 2}px + ${theme.spacing * 4}px)
  );

  ${HeaderMenuLinkSC}:not(:first-of-type) {
    margin-left: 2em;
  }
`;

/**
 * Header settings icon
 */
const HeaderSettingsSC = styled.i`
  position: absolute;
  right: ${theme.spacing * 3}px;
  color: white;
  opacity: 0.25;
  cursor: pointer;

  transition: opacity 0.2s, transform 0.2s;

  &:hover {
    opacity: 0.6;
    transform: rotate(90deg);
  }
`;

export {
  HeaderSC,
  HeaderLogoSC,
  HeaderMenuLeftSC,
  HeaderMenuRightSC,
  HeaderSettingsSC,
};
