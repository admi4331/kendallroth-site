import React from 'react';
import Link from 'gatsby-link';
import PropTypes from 'prop-types';

// Styles
import { HeaderMenuLinkSC } from './styled';

/**
 * Link for Header menu
 * @param {string} to - Link target
 */
const HeaderMenuLink = (props) => {
  const { children, to } = props;

  return (
    <HeaderMenuLinkSC>
      <Link to={to}>{children}</Link>
    </HeaderMenuLinkSC>
  );
};

HeaderMenuLink.propTypes = {
  children: PropTypes.any,
  to: PropTypes.string.isRequired,
};

HeaderMenuLink.defaultProps = {
  children: null,
};

export default HeaderMenuLink;

/* eslint jsx-a11y/anchor-is-valid: off */
