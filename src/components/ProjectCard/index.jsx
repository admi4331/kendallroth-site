import React from 'react';
import PropTypes from 'prop-types';

// Styles
import {
  ProjectCardSC,
  ProjectDescriptionSC,
  ProjectImageSC,
  ProjectImagePlaceholderSC,
  ProjectLinkSC,
  ProjectNameSC,
  ProjectYearSC,
} from './styled';

/**
 * Project section card
 * @param {string} className         - React class name
 * @param {object} project           - Project node
 * @param {string} project.image     - Project image name
 * @param {object} project.imageSize - Project image object (gatsby-image)
 * @param {string} project.name      - Project name
 * @param {string} project.slug      - Project URL slug
 * @param {number} project.year      - Project year
 */
const ProjectCard = (props) => {
  const { className, project } = props;

  const { image, imageSize, name, slug, year } = project;

  const projectUrl = `/projects/${slug}`;
  const hasImage = Boolean(image) && Boolean(imageSize);

  return (
    <ProjectCardSC className={className}>
      {hasImage ? (
        <ProjectImageSC sizes={project.imageSize} />
      ) : (
        <ProjectImagePlaceholderSC />
      )}
      <ProjectDescriptionSC>
        <ProjectNameSC>{name}</ProjectNameSC>
        &nbsp;&ndash;&nbsp;
        <ProjectYearSC>{year}</ProjectYearSC>
        {false && <ProjectLinkSC to={projectUrl}>Details</ProjectLinkSC>}
      </ProjectDescriptionSC>
    </ProjectCardSC>
  );
};

ProjectCard.propTypes = {
  className: PropTypes.string,
  project: PropTypes.shape({
    image: PropTypes.string,
    imageSize: PropTypes.object,
    name: PropTypes.string.isRequired,
    slug: PropTypes.string.isRequired,
    year: PropTypes.number.isRequired,
  }).isRequired,
};

ProjectCard.defaultProps = {
  className: '',
};

export default ProjectCard;
