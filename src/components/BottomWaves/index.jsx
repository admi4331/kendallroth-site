import React from 'react';

// Styles
import { BottomWaveSC, BottomWavesSC, BottomWaveImageSC } from './styled';

/**
 * Index page
 */
const BottomWaves = () => (
  <BottomWavesSC>
    <BottomWaveSC position="top">
      <BottomWaveImageSC position="top" />
    </BottomWaveSC>
    <BottomWaveSC position="mid">
      <BottomWaveImageSC position="mid" />
    </BottomWaveSC>
    <BottomWaveSC position="bot">
      <BottomWaveImageSC position="bot" />
    </BottomWaveSC>
  </BottomWavesSC>
);

export default BottomWaves;
