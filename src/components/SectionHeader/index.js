import React from 'react';
import PropTypes from 'prop-types';

// Styles
import { SectionHeaderSC } from './styled';

const SectionHeader = (props) => {
  const { children, color } = props;

  return <SectionHeaderSC color={color}>{children}</SectionHeaderSC>;
};

SectionHeader.propTypes = {
  children: PropTypes.string.isRequired,
  color: PropTypes.string,
};

SectionHeader.defaultProps = {
  color: 'black',
};

export default SectionHeader;
