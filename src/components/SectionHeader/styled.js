import styled from 'styled-components';

/**
 * Section header
 * @param {string} color - Header color
 */
const SectionHeaderSC = styled.h2`
  font-size: 1.75em;
  color: ${({ color }) => color || 'black'};
  text-align: center;
`;

export { SectionHeaderSC };

/* eslint import/prefer-default-export: off */
