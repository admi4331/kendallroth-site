import styled from 'styled-components';

/**
 * Color picker display
 */
const ColorIndicatorSC = styled.div`
  width: 5em;
  padding: ${({ theme }) => theme.spacing / 2}px;
  border-radius: 4px;
  border: 1px solid ${({ active }) => (active ? 'dodgerblue' : 'lightgrey')};
  background-color: ${({ color }) => color};
  color: ${({ color, theme }) => theme.functions.getContrastColor(color)};
  cursor: pointer;

  ${({ active }) =>
    active &&
    `
    box-shadow: 0px 0px 2px 2px dodgerblue;
  `};
`;

/**
 * Color picker row
 */
const ColorPickerRowSC = styled.div`
  ${ColorIndicatorSC}:not(:first-of-type) {
    margin-left: ${({ theme }) => theme.spacing}px;
  }
`;

export { ColorIndicatorSC, ColorPickerRowSC };
