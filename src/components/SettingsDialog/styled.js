import styled from 'styled-components';

/**
 * Settings dialog container
 */
const SettingsDialogSC = styled.section`
  display: flex;
  flex-direction: column;
  position: fixed;
  right: ${({ theme }) => theme.spacing * 3}px;
  top: calc(
    ${({ theme }) => theme.spacing * 3}px +
      ${({ theme }) => theme.header.height}px
  );
  min-width: 300px;
  padding: ${({ theme }) => theme.spacing * 2}px;
  border-radius: 2px;
  background-color: white;
  box-shadow: 0 0 8px 4px rgba(125, 125, 125, 0.6);

  h3 {
    margin-bottom: 0;
    text-align: center;
  }

  hr {
    height: 1px;
    margin: ${({ theme }) => theme.spacing * 2}px 0;
  }
`;

/**
 * "Interactive" barrier to close dialog when clicked on
 */
const SettingsDialogBackgroundSC = styled.section`
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 100;
`;

/**
 * Settings dialog input label
 */
const SettingsLabelSC = styled.label`
  display: flex;
  align-items: center;
  margin-bottom: ${({ theme }) => theme.spacing}px;

  i {
    margin-left: auto;
    cursor: pointer;

    &:hover {
      color: ${({ theme }) => theme.palette.primary.main};
      font-weight: bold;
    }
  }
`;

export { SettingsDialogSC, SettingsDialogBackgroundSC, SettingsLabelSC };
