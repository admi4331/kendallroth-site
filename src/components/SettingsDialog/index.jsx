import React, { useContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { ChromePicker } from 'react-color';
import { ThemeContext } from 'styled-components';

// Components
import ColorPicker, { ColorPickerRowSC } from './ColorPicker';

// Styles
import {
  SettingsDialogSC,
  SettingsDialogBackgroundSC,
  SettingsLabelSC,
} from './styled';

// Utilities
import { ThemeOverrideContext } from '@utils/theme';

/**
 * Settings dialog for customizing website
 */
const SettingsDialog = (props) => {
  const { onClose } = props;

  const [colors, setColors] = useState({
    background: '',
    dots: '',
    lines: '',
  });
  // Selected color determines what color picker will be affected
  const [selectedColor, setSelectedColor] = useState(null);
  const styledTheme = useContext(ThemeContext);
  const styledThemeOverride = useContext(ThemeOverrideContext);

  // Update the setting color pickers when the theme is changed (initialize or reset)
  useEffect(() => {
    setColors({
      background: styledTheme.palette.background.background,
      dots: styledTheme.palette.background.dots,
      lines: styledTheme.palette.background.lines,
    });
  }, [styledTheme.palette.background]);

  /**
   * Set the selected color for the picker
   * @param {string} key - Selectedd color key
   */
  const onColorSelect = (key) => {
    if (!key || colors[key] === undefined) return;

    setSelectedColor(key);
  };

  /**
   * Update color (from picker)
   * @param {Object} color - Picked color
   * @param {event} event  - React event object
   */
  const onPickerChange = (color) => {
    if (!selectedColor) return;

    const newColors = { ...colors, [selectedColor]: color.hex };

    // "Double update" gives a snappier UI update than solely waiting on theme update
    setColors(newColors);
    styledThemeOverride.updateBackground(newColors);
  };

  const pickerColor = colors[selectedColor];

  return (
    <SettingsDialogBackgroundSC onClick={onClose}>
      <SettingsDialogSC onClick={(e) => e.stopPropagation()}>
        <h3>Customize Site</h3>
        <hr />
        {/* eslint-disable-next-line */}
        <SettingsLabelSC htmlFor="settings__background">
          Background:
          {/* eslint-disable-next-line */}
          <i
            className="material-icons"
            onClick={() => styledThemeOverride.resetBackground()}
          >
            clear
          </i>
        </SettingsLabelSC>
        <ColorPickerRowSC id="settings__background" style={{ display: 'flex' }}>
          {Object.keys(colors).map((key) => (
            <ColorPicker
              key={key}
              active={selectedColor === key}
              color={colors[key]}
              onClick={() => onColorSelect(key)}
            />
          ))}
        </ColorPickerRowSC>
        <hr />
        <span style={{ margin: 'auto' }}>
          <ChromePicker
            color={pickerColor}
            disableAlpha
            onChange={onPickerChange}
          />
        </span>
      </SettingsDialogSC>
    </SettingsDialogBackgroundSC>
  );
};

SettingsDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
};

export default SettingsDialog;
