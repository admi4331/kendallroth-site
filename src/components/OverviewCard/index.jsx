import React from 'react';
import PropTypes from 'prop-types';

// Styles
import {
  OverviewCardSC,
  OverviewCardImageSC,
  OverviewCardTextSC,
  OverviewCardTitleSC,
} from './styled';

/**
 * Card-like display for information
 * @param {element} children        - Card content
 * @param {object}  iconProps       - Icon props
 * @param {object}  iconProps.color - Icon colour
 * @param {object}  iconProps.round - Whether icon is round
 * @param {object}  iconProps.size  - Icon size
 * @param {object}  image           - Card image
 * @param {string}  title           - Card title
 */
const OverviewCard = (props) => {
  const { children, iconProps, image, title } = props;
  const { color: iconColor, round: roundIcon, size: iconSize } = iconProps;

  const hasImage = Boolean(image);
  const hasTitle = Boolean(title);

  const imageSize = iconSize ? { width: iconSize, height: iconSize } : {};

  return (
    <OverviewCardSC>
      {hasImage && (
        <OverviewCardImageSC color={iconColor} round={roundIcon} src={image} {...imageSize} />
      )}
      {hasTitle && <OverviewCardTitleSC>{title}</OverviewCardTitleSC>}
      <OverviewCardTextSC>{children}</OverviewCardTextSC>
    </OverviewCardSC>
  );
};

OverviewCard.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.string]).isRequired,
  iconProps: PropTypes.shape({
    color: PropTypes.string,
    round: PropTypes.bool,
    size: PropTypes.string,
  }),
  image: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  title: PropTypes.string,
};

OverviewCard.defaultProps = {
  iconProps: {
    color: 'black',
    round: true,
    size: null,
  },
  image: null,
  title: null,
};

export default OverviewCard;
