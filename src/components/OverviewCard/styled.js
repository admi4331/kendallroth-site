import styled, { css } from 'styled-components';

/**
 * Information card wrapper
 */
const OverviewCardSC = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 1em;
  background-color: rgba(255, 255, 255, 0.95);
  border-radius: 2px;
  box-shadow: ${({ theme }) => theme.shadows[2]};
`;

/**
 * Card content text
 */
const OverviewCardTextSC = styled.div`
  font-size: 0.9em;
`;

/**
 * Card content image
 * @param {string}  color - Image colour
 * @param {boolean} round - Whether image is round
 */
const OverviewCardImageSC = styled.img`
  ${(props) =>
    props.color &&
    css`
      background-color: ${props.color};
    `};

  ${(props) =>
    props.round &&
    css`
      border-radius: 100%;
    `};
`;

/**
 * Card content title
 */
const OverviewCardTitleSC = styled.div`
  margin-bottom: 0.5em;
  text-align: center;
  font-weight: bold;
`;

export { OverviewCardSC, OverviewCardImageSC, OverviewCardTextSC, OverviewCardTitleSC };
