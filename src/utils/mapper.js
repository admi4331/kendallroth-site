/**
 * Map a query set of files to their matching images
 *
 * @param  {Object[]} fileEdges  - Markdown files
 * @param  {Object[]} imageEdges - Image files
 * @return {Object[]}            - List of mapped files and their images
 *
 * NOTE: File must be a markdown file with an "image" attribute.
 *         Image queries must have a "childImages" field for Gatsby.
 */
const mapFileToImageFromQuery = (fileEdges, imageEdges) => {
  return fileEdges.reduce((accum, fileEdge) => {
    const { childMarkdownRemark } = fileEdge.node;

    // Only include valid markdown files
    if (!childMarkdownRemark) return accum;

    // Only include published files
    if (!childMarkdownRemark.frontmatter.published) return accum;

    // Map files with their corresponding images
    const edgeNode = { ...childMarkdownRemark.frontmatter };

    const matchingImageEdge = imageEdges.find((i) => {
      if (!i.node.childImageSharp) return false;
      return i.node.childImageSharp.sizes.originalName === edgeNode.image;
    });

    // Remove image from the file if not found
    if (!matchingImageEdge) {
      return [
        ...accum,
        {
          ...edgeNode,
          image: null,
        },
      ];
    }

    // The "sizes" field is provided by "ImageSharp" to render different
    //   images per screen size (required by Gatsby Image component).
    const { sizes } = matchingImageEdge.node.childImageSharp;

    return [
      ...accum,
      {
        ...edgeNode,
        imageSize: sizes,
      },
    ];
  }, []);
};

export { mapFileToImageFromQuery };

/* eslint import/prefer-default-export: off */
