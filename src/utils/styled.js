import styled, { css } from 'styled-components';

// Utils
import theme from '@utils/theme';

/**
 * Page container
 * @param {boolean} offsetTop - Whether page is offset to account for fixed header
 */
const PageSC = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;

  ${(props) =>
    (props.offsetTop === undefined || props.offsetTop) === true &&
    css`
      padding-top: ${theme.header.height}px;
    `};
`;

export { PageSC };

/* eslint import/prefer-default-export: off */
