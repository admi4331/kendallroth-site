import React from 'react';
import PropTypes from 'prop-types';

// Components
import OverviewCard from '@components/OverviewCard';

// Utilities
import analysisLogo from '@assets/icons/logo_analysis.svg';
import designLogo from '@assets/icons/logo_design.svg';
import developmentLogo from '@assets/icons/logo_development.svg';

// Styles
import {
  CardRowSC,
  SectionHeaderSC,
  SectionQuoteSC,
  WhatIDoSC,
} from './styled';

/**
 * Index page
 */
const WhatIDoSection = (props) => {
  const { sectionRef } = props;

  // "What I Do" card icons
  const iconProps = {
    round: true,
    size: '100px',
  };

  return (
    <WhatIDoSC ref={sectionRef}>
      <SectionHeaderSC>What I Do</SectionHeaderSC>
      <SectionQuoteSC>
        I&lsquo;m passionate about web design and development, focusing on clean
        and simple design/functionality. I strive to be a dedicated team player
        while adapting to different environments and tasks.
      </SectionQuoteSC>
      <CardRowSC>
        <OverviewCard
          iconProps={{ ...iconProps, color: '#ec6d1b' }}
          image={analysisLogo}
          title="Analysis"
        >
          Careful analysis, including data modeling and project management, is a
          necessity for project planning and plays a vital role in my work.
        </OverviewCard>
        <OverviewCard
          iconProps={{ ...iconProps, color: '#1999e2' }}
          image={designLogo}
          title="Design"
        >
          While not a designer, I do enjoy creating my own content and
          illustrations while striving to follow a simple and minimalistic
          approach.
        </OverviewCard>
        <OverviewCard
          iconProps={{ ...iconProps, color: '#d52a2a' }}
          image={developmentLogo}
          title="Development"
        >
          The whole development cycle&mdash;whether front-end, back-end,
          database, documentation, etc.&mdash;all receive the same careful
          attention to detail.
        </OverviewCard>
      </CardRowSC>
    </WhatIDoSC>
  );
};

WhatIDoSection.propTypes = {
  sectionRef: PropTypes.object.isRequired,
};

export default WhatIDoSection;
