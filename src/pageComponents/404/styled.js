import styled from 'styled-components';
import Link from 'gatsby-link';

// Styles
import { MediaQuery } from '@utils/mediaQuery';

/**
 * Link to return to home page
 */
const HomePageLinkSC = styled(Link)`
  display: inline-block;
  margin-top: ${({ theme }) => theme.spacing * 3}px;
  padding: ${({ theme }) => `${theme.spacing * 1.5}px ${theme.spacing * 3}px`};
  color: ${({ theme }) => theme.palette.white};
  text-decoration: none;
  background-color: ${({ theme }) => theme.palette.primary.main};
  border-radius: 4px;

  transition: background-color 0.2s, border-radius 0.1s;

  &:hover {
    background-color: ${({ theme }) => theme.palette.primary.light};
    border-radius: 100px;
  }
`;

/**
 * 404 content wrapper
 */
const NotFoundSC = styled.section`
  max-width: 600px;
  margin: 5em 2.5em;
  align-self: center;
  text-align: center;
  color: white;
  font-size: 1.5em;
`;

/**
 * 404 title
 */
const NotFoundTitleSC = styled.h1`
  color: ${({ theme }) => theme.palette.secondary.main};
  font-size: 4em;
  margin: 0 0 0.25em 0;

  ${MediaQuery.tabletUp`
    font-size: 6em;
  `}
`;

export { HomePageLinkSC, NotFoundSC, NotFoundTitleSC };

/* eslint import/prefer-default-export: off */
